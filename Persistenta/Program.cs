﻿using System;
using System.Collections.Generic;

namespace Persistenta
{
    /**
     * persistence(39) == 3
     * because 3*9 = 27, 2*7 = 14, 1*4=4 and 4 has only one digit
     *
     * persistence(999) == 4
     * because 9*9*9 = 729, 7*2*9 = 126, 1*2*6 = 12, and finally 1*2 = 2
     *
     * persistence(4) == 0 // because 4 is already a one-digit number
     */
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine(Persistenta(25));
            
            Console.ReadKey();
        }

        public static int Persistenta(int n)
        {
            if (n < 10)
                return 0;
            
            int persistenta = 0;

            while (n >= 10)
            {
                persistenta++;
                
                List<int> digits = new List<int>();
                while (n > 0)
                {
                    digits.Add(n % 10);
                    n = n / 10;
                }

                n = 1;

                foreach (var digit in digits)
                {
                    n = n * digit;
                }  
            }
            
            return persistenta;
        }
    }
}