﻿using System;
using NUnit.Framework;
using Persistenta;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
            Console.WriteLine("****** Basic Tests");    
            Assert.AreEqual(3, Program.Persistenta(39));
            Assert.AreEqual(0, Program.Persistenta(4));
            Assert.AreEqual(2, Program.Persistenta(25));
            Assert.AreEqual(4, Program.Persistenta(999));
        }
    }
}